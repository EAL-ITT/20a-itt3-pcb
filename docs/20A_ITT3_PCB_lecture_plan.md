---
title: '20A ITT3 PCB'
subtitle: 'Lecture plan'
filename: '20A_ITT3_PCB_lecture_plan'
authors: ['Nikolaj Simonsen \<nisi@ucl.dk\>']
main_author: 'Nikolaj Simonsen'
date: 2020-06-15
email: 'nisi@ucl.dk'
left-header: \today
right-header: Lecture plan
skip-toc: false
---


# Lecture plan

The lecture plan consists of the following parts: a week/lecture plan, an overview of the distribution of study activities and general information about the course, module or project.

* Study program, class and semester: IT technology, oeait19, (3rd semester)
* Name of lecturer and date of filling in form: NISI 2020-06-15
* Title of the course, module or project and ECTS: Elective subject (PCB), 5 ECTS
* Required readings, literature or technical instructions and other background material: None

See weekly plan for details like detailed daily plan, links, references, exercises and so on.

| INIT | Week  | Content                                            |
|:---- |:----- |:-------------------------------------------------- |
| NISI | 35    | Introduction & circuit decision  | 
| NISI | 36    | Component selection  | 
| NISI | 37    | Nordcad workshop, Schematic, simulation, testing, footprints         |
| NISI | 38    | Design rule check, netlist, board layout           |
| NISI | 39    | Design for manufacturing, Layers, Routing, gerber and drill files, Factory design constraints, ordering PCB's            |
| NISI | 40    | Kicad        |
| NISI | 41    | PCB Assembly                     |
| NISI | 43    | Scientific poster, how to pitch, Exam      |


# General info about the course, module or project

The purpose of the course is to learn about the PCB design and manufacturing proces.  
During the course the student will design and manufacture at least one PCB, preferrable two. 
Students will deside on the circuit they want to design, manufacture and test.
The exam at the end of the course is a poster presentation where students present their knowledge about the PCB process as well as their produced PCB(s) 

## The student’s learning outcome

At the end of the course, the student will be able to design and manufacture simple PCB's using both the OrCad suite and KiCad as well as knowing the terminology used in the industry.

## Content

The subject area includes fundamentals of PCB design and manufacturing, the use of EDA software as well as design, development, testing and documentation of PCB's.

## Method

The course focuses on practice mixed with applied theory.  
Theory will take place on Tuesday mornings and the students will work independently Tuesday afternoon as well as the entire Wednesday.  
Practice is very important when designing PCB's and the proces is time consuming. It is therefore crucial that students spend all working hours to gain the neccesary knowledge from the course.   
To facilitate different learning styles video material supporting the content will be made available when possible.  

## Equipment

Windows computer with OrCad and Nordcad student license as well as KiCad installed.

## Projects with external collaborators  

Nordcad workshop (Martin Lange Nonboe, Nordcad)
Design for manufacturing (Søren Knudsen, Universal Robots)

## Test form/assessment

### Description

The exam is a poster presentation where students present the process of designing their
PCB as well as a presentation of the physical product (populated PCB).  
The entire exam has a duration of 2,5 hours where students will present to anyone interested.  
During the exam students will present to a lecturer for maximum 5 minutes.  

Grading is 7 grade scale.  
The exam is with internal sensor.  
Contact person for this exam is Nikolaj Simonsen, nisi@ucl.dk

### Hand-in

Students are expected to hand-in on wiseflow:

* Front page with picture of PCB
* Pitch preparation
* Link to gitlab repository

## Other general information

None at this time.
