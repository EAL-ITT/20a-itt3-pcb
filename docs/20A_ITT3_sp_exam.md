---
title: '20A ITT3 SP'
subtitle: 'Exam description'
filename: '20A_ITT3_sp_exam'
authors: ['Peter Thomsen \<peth.edu@gmail.com\>']
main_author: 'Peter Thomsen'
date: 2020-11-25
email: 'peth.edu@gmail.com\'
left-header: \today
right-header: Links
skip-toc: false
---

# Document content

This document describes the exam for the SP elective course.

# Exam description  

The exam is a poster presentation.
The students are examined in groups.  
They will have 10 min. to present the subjects and then 10 minutes for questions.  
The examination will only be concerned with the subjects taught in this curriculum, such as: 

* OSPF
* BGP
* MPLS
* LDP
* L3VPN
* VPLS

Grades are given individually for students in the groups, based on their knowledge of the various subjects.
 
The exam takes place at Seebladsgade 1, 5000 Odense.  
Room number is announced at Wiseflow

There will be cardboard walls available for the posters.   

# Grading

Grading is 7 grade scale.  
The exam is with internal sensor. 

# Hand-in requirements

* None

# Exam dates

1st attempt **December 11th 2020 9:30 – 12:30**    
2nd attempt **January 7th 2021 9:30 - 12:30**  
3rd attempt **January 28th 2020 9:30 - 12:30**  


# Additional information

See the [https://www.ucl.dk/international/full-degree/study-documents#it+technology](https://www.ucl.dk/international/full-degree/study-documents#it+technology)