---
Week: 35
Content:  Introduction & circuit decision
Material: See links in weekly plan
Initials: NISI
---

# Week 35 - Introduction & circuit decision

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals
* Setup libraries in Orcad capture

### Learning goals
* The student will have an overview of the Orcad suite
* The student can create an Orcad project
* The student can create a schematic in Orcad capture

## Deliverables
* Gitlab project created with meaningful readme and folder structure

## Schedule Tuesday

Lectures will be at zoom in room: **67929688905, password: 1234**

* 09:00 Introduction to the course
* 10:30'ish Work on deliverables and exercises
* 15:30 End of day

## Schedule Wednesday

Contact nisi if you have questions or need at meeting online

* 08:15 Work on deliverables and exercises
* 15:30 End of day

## Hands-on time

### Exercise 0 - Setup gitlab project

Setup a gitlab project in the gitlab group [https://gitlab.com/20a-itt3-pcb-students-group](https://gitlab.com/20a-itt3-pcb-students-group) and use the naming **ITT3-PCB-YOURNAME** for the project

The group is private so ask nisi to include you in the group.

### Exercise 1 - Project log

Create a readme.md document in your gitlab project where you log your experience about the PCB creation process.  
This is mandatory and will be valuable when you have to prepare a pitch for the exam.

The log shall, as a minimum, for each working day include:

1. Summary of activities
2. Lessons learned
3. Ressources used

### Exercise 2 - Circuit research

* Research and decide on the circuit you want to design and build
* Create a block diagram with logical subcircuits and specifications for input and output values between subcircuits.  
* Document with a schematic drawn in Orcad capture.
* If needed, identify and document test points and their values. 

It is okay to chose a design that you will use in the ITT3 project.

If you do not have a circuit chosen by Tuesday week 36 I will as you to do a circuit I choose. We have a need for some sensor pcb's utilizing Bosch sensors. 
You can read about them at Bosch sensortech [https://www.bosch-sensortec.com/products/](https://www.bosch-sensortec.com/products/)

## Comments

[Introduction slides](https://docs.google.com/presentation/d/1YsukOngrXMKDU_IIcyL57_2fBpfOFJNsKgQTgA8SeXY/edit?usp=sharing)