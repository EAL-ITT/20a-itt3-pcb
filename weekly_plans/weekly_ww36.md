---
Week: 36
Content:  Component selection
Material: See links in weekly plan
Initials: NISI
---

# Week 36 - Component selection

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals
* Nordcad tutorials completed

### Learning goals
* The student will have a deeper insight into Orcad

## Deliverables
* Nordcad beginners course documented on gitlab
* Components chosen and datasheets documented on gitlab

## Schedule Tuesday

* 08:15 Work on deliverables and exercises
* 15:30 End of day

## Schedule Wednesday

* 08:15 Work on deliverables and exercises
* 15:30 End of day

## Hands-on time

### Exercise 0 - Component research

* Research which components you want to use
* Document your research in a document with reflections on the chosen components. Why did you choose the specific variant, smt vs. leaded, power rating etc. 
* Gather datasheets for all chosen components and include them in the gitlab project

### Exercise 1 - Nordcad beginners course

Complete the Nordcad beginners course located at [https://www.nordcad.eu/student-forum/beginners-course/](https://www.nordcad.eu/student-forum/beginners-course/)

The course have 3 sections: Schematic, Simulation and PCB design.
You need to complete all 3 sections as a preparation for next weeks Nordcad workshop.

Document each Orcad project along with your notes (readme.md) in your Gitlab project.

Creating a .gitignore file is a good idea to avoid storing unnessesary
files at gitlab [https://www.toptal.com/developers/gitignore/api/orcad](https://www.toptal.com/developers/gitignore/api/orcad)

## Comments

This week is without lecturer because of collaboration days with first semester. Questions on element (riot) or mail will be answered asap