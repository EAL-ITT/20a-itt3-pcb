---
Week: 37
Content:  Orcad Capture + Nordcad workshop
Material: See links in weekly plan
Initials: NISI
---

# Week 37 - Orcad Capture + Nordcad workshop

## Goals of the week(s)
Pratical and learning goals for the period is as follows

### Practical goals

* Participate in Nordcad workshop
* Simulation or physical tests performed and documented
* Footprints created and assigned in Capture

### Learning goals
* The student can reason about a given circuit
* The student has knowledge about how to convert datasheet specs to footprints
* The student can create padstacks in OrCad 
* The student can create footprints in OrCad

## Deliverables
* Relevant circuit footprints included in Gitlab project
* Log included in Gitlab project

## Schedule Tuesday

Lectures will be at zoom in room: **67929688905, password: 1234**

* 09:00 Introduction to the day
* 09:15 Circuit presentations
    * Everyone tells about their chosen circuit and it's functionality
* 10:00 Nordcad workshop - Online in the zoom room
* 15:30 End of day

## Schedule Wednesday

Contact nisi if you have questions or need at meeting online

* 08:15 Work on deliverables and exercises
* 15:30 End of day

## Hands-on time

### Exercise 0 - Prepare schematic for simulation

In the Norcad workshop Tuesday you did som simulations, if in doubt recap those or use the exercises at [https://www.nordcad.eu/student-forum/exercises/](https://www.nordcad.eu/student-forum/exercises/) to recap

1. Make sure you have selected components with at pspice profile attached. All components in the pspice menu have this.
2. Make a list of simulations you want to perform (input and output as a minimum), note them in your log
3. Perform the simulations and capture the results (simulation profile settings and simulated waveforms) in your log or seperate document.

### Exercise 1 - Assign footprints

Before preapring your schematic for PCB you might want to create a copy of the Schematic. The process of changing a simulation schematic to a schematic with footprints might inable you to run the simulations again. 

Watch the video 
OrCAD 17.2 PCB Design Tutorial - 04 - Capture: Preparing for Manufacture[https://www.youtube.com/watch?v=zFTEcsgR8iE&list=PLDclr_SCaTAwUiJYgKDOUQvv6TJRj6fXx&index=4](https://www.youtube.com/watch?v=zFTEcsgR8iE&list=PLDclr_SCaTAwUiJYgKDOUQvv6TJRj6fXx&index=4)

1. Gather footprints for your circuit (see ressources in comments)  Instructions [https://www.youtube.com/watch?v=CYtn_WWPizo&list=PLDclr_SCaTAwUiJYgKDOUQvv6TJRj6fXx&index=6](https://www.youtube.com/watch?v=CYtn_WWPizo&list=PLDclr_SCaTAwUiJYgKDOUQvv6TJRj6fXx&index=6) 
2. Examine the footprints to make sure they meet the specs in the datasheet.  You might need to change the footprint? [https://youtu.be/s8mYUTix3ao](https://youtu.be/s8mYUTix3ao)
3. If you can't find a footprint online you will have to make it yourself - video 8 - 14 in this playlist has instructions [https://www.youtube.com/playlist?list=PLDclr_SCaTAwUiJYgKDOUQvv6TJRj6fXx](https://www.youtube.com/playlist?list=PLDclr_SCaTAwUiJYgKDOUQvv6TJRj6fXx) 


## Comments

Footprint suppliers:
Ultralibrarian [https://www.ultralibrarian.com/](https://www.ultralibrarian.com/)  
Componentsearchengine [http://componentsearchengine.com/](http://componentsearchengine.com/)  
Octopart [https://octopart.com/](https://octopart.com/)  
Others: TI, Mouser, Farnell etc.